from setuptools import find_packages, setup

setup(
    name='nose-hackathon',
    version='0.1.1',
    description='Plugin for hackathon',
    author='Daniel Anggrianto',
    author_email='d.anggrianto@gmail.com',
    packages=find_packages(exclude=["tests"]),
    entry_points={
        'nose.plugins.0.10': [
            'nose_hackathon = nose_hackathon.plugin:NoseHackathon'
        ]
    },
)
